import { Injectable } from '@angular/core';

@Injectable()
export class BmiService {

  tmpNumber: number = 100;

  constructor() { }

  calBMI(h: number, w: number): number {
    let bmi: number = 0.0;
    bmi = w / Math.pow((h / 100), 2);

    return bmi;
  }

}
