import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-ng-model-driven',
  templateUrl: './ng-model-driven.component.html',
  styleUrls: ['./ng-model-driven.component.css']
})
export class NgModelDrivenComponent implements OnInit {

  constructor(private formBuilder: FormBuilder) { }

  userForm: FormGroup;

  ngOnInit() {
    this.userForm = this.formBuilder.group({
      name: [null, [Validators.required, Validators.minLength(4), Validators.maxLength(10)]],
      email: [],
      address: this.formBuilder.group({
        street: [],
        city: [],
        postalcode: [null, [Validators.pattern('^[1-9][0-9]{4}')]]
      })
    })
  }

  onSubmit() {
    console.log(this.userForm.value);
  }

}
