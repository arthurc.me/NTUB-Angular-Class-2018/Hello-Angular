import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';

@Component({
  selector: 'app-callhttp',
  templateUrl: './callhttp.component.html',
  styleUrls: ['./callhttp.component.css']
})
export class CallhttpComponent implements OnInit {

  d: any;
  postBody: object = {
    title: '',
    author: ''
  }

  constructor(private httpService: HttpService) { }

  ngOnInit() {
  }

  get() {
    this.httpService.getData('posts').subscribe(
      data => {
        this.d = data;
      },
      error => {

      },
      () => {
        console.log('finish');
      }
    )
  }

  post() {
    this.httpService.postData("posts", this.postBody).subscribe(
      data => {
        this.d = data;
      },
      error => {

      },
      () => {
        console.log('finish');
      }
    )
  }
}
