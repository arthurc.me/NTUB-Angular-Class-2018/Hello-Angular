import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appHightlight]'
})
export class HightlightDirective {

  @Input() c: string = 'yellow'

  el: HTMLElement

  constructor(el:ElementRef) {
    this.el = el.nativeElement
  }

  @HostListener('mouseenter') onMouseEnter() {
    this.el.style.backgroundColor = this.c
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.el.style.backgroundColor = null
  }

}
