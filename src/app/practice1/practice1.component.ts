import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-practice1',
  templateUrl: './practice1.component.html',
  styleUrls: ['./practice1.component.css']
})
export class Practice1Component implements OnInit {

  dictionary: Dictionary = {
    english: '',
    chinese: ''
  }

  dictionaries: Dictionary[] = []

  constructor() { }

  ngOnInit() {
  }

  save() {
    console.log(this.dictionary)

    if (!this.dictionary.english || !this.dictionary.chinese) {
      alert('Empty!!!')
      return
    }

    let dictionaryCopy = Object.assign({}, this.dictionary)

    this.dictionaries.push(dictionaryCopy)
    this.dictionary = {
      english: '',
      chinese: ''
    }
  }

}
export interface Dictionary {
  english: string,
  chinese: string,
}
