import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lesson6',
  templateUrl: './lesson6.component.html',
  styleUrls: ['./lesson6.component.css']
})
export class Lesson6Component implements OnInit {

  score: number = 0

  constructor() { }

  ngOnInit() {
  }

  onScoreChange(event: number) {
    this.score = event
  }

  click() {
    console.log('Hello')
  }
}
