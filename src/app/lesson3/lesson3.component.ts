import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lesson3',
  templateUrl: './lesson3.component.html',
  styleUrls: ['./lesson3.component.css']
})
export class Lesson3Component implements OnInit {

  scoreList: number[] = [10, 20, 30, 40, 50, 60, 70, 80]
  now: Date = new Date()

  account: Account = {
    account: 'arthurc0102',
    password: 'P@ssw0rd'
  }

  constructor() { }

  ngOnInit() {
  }

}
export interface Account {
  account: string,
  password: string
}
