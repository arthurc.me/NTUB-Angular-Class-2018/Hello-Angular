import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-dyclass',
  templateUrl: './dyclass.component.html',
  styleUrls: ['./dyclass.component.css']
})
export class DyclassComponent implements OnInit {
  @ViewChild('pc') pc:ElementRef;

  number1: number;
  number2: number;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.number1 = +this.route.snapshot.params.number1;
    this.number2 = +this.route.snapshot.params.number2;
  }

  addRdd() {
    this.pc.nativeElement.classList.add('rdd');
  }

}
